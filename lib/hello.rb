require 'greeter'

# Default is "World"
# Author: ScampyOwl (scamper@gmx.net)
name = ARGV.first || "World"

greeter = Greeter.new(name)
puts greeter.greet